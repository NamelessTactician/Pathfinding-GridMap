﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public static class MapGenerator {

    public static List<List<Node>> generateNodes(int width, int height)
    {
        List<List<Node>> tileMap = new List<List<Node>>();
        System.Random rnd = new System.Random();
        for (int y = 0; y <= height - 1; y++)
        {
            List<Node> tempRow = new List<Node>();
            for (int x = 0; x <= width - 1; x++)
            {
                Node tempNode = new Node(x, y, rnd.Next(1, 4));
                tempRow.Add(tempNode);
            }
            tileMap.Add(tempRow);
        }
        //after generating map generate neighbours for every tail in the map
        generateVirtualNeighbours(tileMap);
        return tileMap;
    }

    public static List<List<UnityNode>> createUnityMap(List<List<Node>> tileMap,GameObject tilePrefab,Canvas unityMapCanvas)
    {
        List<List<UnityNode>> tempMap = new List<List<UnityNode>>();
        foreach (var row in tileMap)
        {
            List<UnityNode> tempRow = new List<UnityNode>();
            foreach (var node in row)
            {
                UnityNode tempUnityNode = ((GameObject)UnityEngine.Object.Instantiate(tilePrefab,new Vector3(),Quaternion.Euler(new Vector3()))).GetComponent<UnityNode>();
                tempUnityNode.transform.SetParent(unityMapCanvas.transform);
                ((RectTransform)tempUnityNode.transform).anchoredPosition = new Vector2(node.x* ((RectTransform)tempUnityNode.transform).sizeDelta.x, node.y* ((RectTransform)tempUnityNode.transform).sizeDelta.y);
                tempUnityNode.M = node.m;
                tempUnityNode.X = node.x;
                tempUnityNode.Y = node.y;
                tempUnityNode.Passable = true;
                tempRow.Add(tempUnityNode);
            }
            tempMap.Add(tempRow);
        }
        //after generating map generate neighbours for every tail in the map
        generateUnityNeighbours(tempMap);
        return tempMap;
        
    }


    private static void generateVirtualNeighbours(List<List<Node>> tileMap)
    {
        foreach (var row in tileMap)
        {
            foreach (var node in row)
            {
                //add           [LU][ U  ][RU]
                //neighbours    [L ][node][R ]
                //              [LD][ D  ][RD]

                //add[L ]
                if (node.x >= 1)
                    node.neighbours.Add(tileMap[node.y][node.x - 1]);
                //add[R ]
                if (node.x <= row.Count - 2)
                    node.neighbours.Add(tileMap[node.y][node.x + 1]);
                //[D]
                if (node.y >= 1)
                    node.neighbours.Add(tileMap[node.y - 1][node.x]);
                //[U]
                if (node.y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.y + 1][node.x]);
                //[LU]
                if (node.x >= 1 && node.y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.y + 1][node.x - 1]);
                //[LD]
                if (node.x >= 1 && node.y >= 1)
                    node.neighbours.Add(tileMap[node.y - 1][node.x - 1]);
                //[RU]
                if (node.x <= row.Count - 2 && node.y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.y + 1][node.x + 1]);
                //[RD]
                if (node.x <= row.Count - 2 && node.y >= 1)
                    node.neighbours.Add(tileMap[node.y - 1][node.x + 1]);
            }
        }
    }
    private static void generateUnityNeighbours(List<List<UnityNode>> tileMap)
    {
        foreach (var row in tileMap)
        {
            foreach (var node in row)
            {
                //add           [LU][ U  ][RU]
                //neighbours    [L ][node][R ]
                //              [LD][ D  ][RD]

                //add[L ]
                if (node.X >= 1)
                    node.neighbours.Add(tileMap[node.Y][node.X - 1]);
                //add[R ]
                if (node.X <= row.Count - 2)
                    node.neighbours.Add(tileMap[node.Y][node.X + 1]);
                //[D]
                if (node.Y >= 1)
                    node.neighbours.Add(tileMap[node.Y - 1][node.X]);
                //[U]
                if (node.Y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.Y + 1][node.X]);
                //[LU]
                if (node.X >= 1 && node.Y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.Y + 1][node.X - 1]);
                //[LD]
                if (node.X >= 1 && node.Y >= 1)
                    node.neighbours.Add(tileMap[node.Y - 1][node.X - 1]);
                //[RU]
                if (node.X <= row.Count - 2 && node.Y <= tileMap.Count - 2)
                    node.neighbours.Add(tileMap[node.Y + 1][node.X + 1]);
                //[RD]
                if (node.X <= row.Count - 2 && node.Y >= 1)
                    node.neighbours.Add(tileMap[node.Y - 1][node.X + 1]);
            }
        }
    }

    public static void displayUnityNeighbours(UnityNode node)
    {
        for (int i = 0; i < node.neighbours.Count; i++)
        {
            setUnityParentPath(node, node.neighbours[i]);
        }
    }

    private static void createHeuristicDistance(List<List<Node>> tileMap, Node goal)
    {
        foreach (var row in tileMap)
        {
            foreach (var node in row)
            {
                node.h = Math.Abs(node.x - goal.x) + Math.Abs(node.y - goal.y);
            }
        }
    }
    private static void createUnityHeuristicDistance(List<List<UnityNode>> tileMap, UnityNode goal)
    {
        goal.GetComponent<Image>().color = Color.yellow;
        foreach (var row in tileMap)
        {
            foreach (var node in row)
            {
                node.H = Math.Abs(node.X - goal.X) + Math.Abs(node.Y - goal.Y);
            }
        }
    }

    private static int calculateUnityGvalue(UnityNode parent_start, UnityNode goalOfmovement)
    {
        return goalOfmovement.G = parent_start.G + goalOfmovement.M; 
    }

    public static void calculateUnityPath(UnityNode start, UnityNode goal, List<List<UnityNode>> tileMap)
    {
        List<UnityNode> open = new List<UnityNode>();
        List<UnityNode> closed = new List<UnityNode>();
        UnityNode tempLowestFnode = start.neighbours[0];
        int numberOfloopIterations = 0;
        //we count heuristics for our destination node
        createUnityHeuristicDistance(tileMap, goal);
        open.AddRange(start.neighbours);
        closed.Add(start);
        //we should clear G value in case there already was some calculated for our start node
        //we want our start node's G value to be 0
        start.G = 0; 

        foreach (var unityNode in open)
        {
            //setting G value for all neighbours (F will automatically update)
            //setting start node as parent to it's neighbours
            unityNode.G = calculateUnityGvalue(start, unityNode);
            setUnityParentPath(start, unityNode);
        }
              
        //this will repeat and  we will assign tempLowestFnode with node with lowest F value
        while (tempLowestFnode != goal && numberOfloopIterations<200){
            tempLowestFnode = open[0];
            foreach (var unityNode in open)
            {
                //F value is set in Update_F_value methot within UnityNode class, method is lunched each time G value is changed
                //compare every neighbour to previous one if it's F value is lower then save it to be next for comparison
                if (unityNode.F > tempLowestFnode.F || unityNode.Passable == false)
                {
                    continue;
                }
                else
                {
                    tempLowestFnode = unityNode;  
                }
            }
            Debug.Log(string.Format("Node with lowest F: X: {0} , Y: {1} ",tempLowestFnode.X,tempLowestFnode.Y));
            //when we find the lowest F node then we add it to close list and  remove it from open list
            closed.Add(tempLowestFnode);
            open.Remove(tempLowestFnode);
            //we have lowest F value node now stored in tempLowestFnode and we need to chceck it's neighbours 
            //we calculate G cost to getting to that node from current node (tempLowestFnode) if it is less than getting to it from it's previous parent
            //we change its parent to our current node

            foreach (var neighbour in tempLowestFnode.neighbours)
            {
                //if neighbour node is impassible or in closed list ignore it and take another neighnbour to check
                if (neighbour.Passable == false || closed.Contains(neighbour))   //not sure if I should check for closed list here because traveling to this node from current tempLowestFnode could be cheaper than going to it from other node
                { continue; }
                //if neighbour is in open list check if route from current parent node is shorter than it's G value if so set parent to current node
                else if (neighbour == goal)
                {
                    setUnityParentPath(tempLowestFnode, neighbour);
                    neighbour.G = calculateUnityGvalue(tempLowestFnode, neighbour);
                    //here we should retrack through parents to start node creating list of UnityNodes called path
                    Debug.Log(string.Format("Finish algorithm!"));
                    return;
                }
                //if neighbour that we are checking is in open list
                else if (open.Contains(neighbour))
                {
                    //and if going to it costs less than it's previous path then change path to this one (from currently checked tempLowestFnode)
                    if (calculateUnityGvalue(tempLowestFnode, neighbour) < neighbour.G)
                    {
                        setUnityParentPath(tempLowestFnode, neighbour);
                        neighbour.G = calculateUnityGvalue(tempLowestFnode, neighbour);
                    }
                }                 
                //if neighbour is not on open or closed list and is passable add it to open list parent it to current node and calculate it's G and F value
                else
                {
                    setUnityParentPath(tempLowestFnode, neighbour);
                    neighbour.G = calculateUnityGvalue(tempLowestFnode, neighbour);
                    open.Add(neighbour);
                }
            }
            //we end to check neighbours of tempLowestFnode now we need to find second lowest F value node in open list and check it's neighbours 
            numberOfloopIterations++;
        }
    }
    //need to finish this method
    public static void calculateUnityRangeOfMovement(UnityNode start, int movementPoints, List<List<UnityNode>> tileMap)
    {
        int currentMovementPoints = movementPoints;
        UnityNode currentUnityNode = start;
        List<UnityNode> open = new List<UnityNode>();
        List<UnityNode> closed = new List<UnityNode>();
        List<UnityNode> possibleMovementNodes = new List<UnityNode>();
        //in case there is any value assigned to G we don't want it to calculate wrong path
        start.G = 0;

        foreach (var neighbour in  currentUnityNode.neighbours)
        {
            if (open.Contains(neighbour))
            {
                continue;
            }
            else
            {
                neighbour.G = calculateUnityGvalue(currentUnityNode, neighbour);
                if (currentMovementPoints >= neighbour.G)
                {
                    open.Add(neighbour);
                    possibleMovementNodes.Add(neighbour);
                }
            }
                
        }
    }

    private static void setUnityParentPath(UnityNode parent, UnityNode child)
    {
        child.parentPath = parent;
        parent.GetComponent<Image>().color = Color.yellow;
        child.RTArrow.SetActive(false);
        child.rightArrow.SetActive(false);
        child.RBArrow.SetActive(false);
        child.topArrow.SetActive(false);
        child.botArrow.SetActive(false);
        child.LTArrow.SetActive(false);
        child.leftArrow.SetActive(false);
        child.LBArrow.SetActive(false);
        // deactivate all arrows

        if (parent.X < child.X)
        {
            if (parent.Y < child.Y)
            {
                child.LBArrow.SetActive(true);
            }
            if (parent.Y == child.Y)
            {
                child.leftArrow.SetActive(true);
            }
            if (parent.Y > child.Y)
            {
                child.LTArrow.SetActive(true);
            }
        }
        else if (parent.X == child.X)
        {
            if (parent.Y < child.Y)
            {
                child.botArrow.SetActive(true);
            }
            if (parent.Y > child.Y)
            {
                child.topArrow.SetActive(true);
            }
        }
        else if (parent.X > child.X)
        {
            if (parent.Y < child.Y)
            {
                child.RBArrow.SetActive(true);
            }
            if (parent.Y == child.Y)
            {
                child.rightArrow.SetActive(true);
            }
            if (parent.Y > child.Y)
            {
                child.RTArrow.SetActive(true);
            }
        }

    }
 
}
