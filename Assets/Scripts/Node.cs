﻿using System;
using System.Collections.Generic;


   public class Node
    {
       public Node(int xValue, int yValue,int moveCost)
        {
            x = xValue;
            y = yValue;
            m = moveCost;
            passable = true;
        }
    public Node lastVisitedNode; //for pathfinding node from which we came to this node 
       // Node (int xValue,int yValue, int moveCost,)
    public List<Node> neighbours = new List<Node>();
    public int f;  //f=g+h
    public int h; // number of nodes to pass till you get to target only vertical and horizontal neighbours are evaluated
    public int g;
    public bool passable;
    public int x;
    public int y;
    public int m;   //movement cost

    }

