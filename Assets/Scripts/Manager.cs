﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Manager : MonoBehaviour
{
    public List<List<Node>> virtualTileMap = new List<List<Node>>();
    public List<List<UnityNode>> unityTileMap = new List<List<UnityNode>>();
    public Canvas unityMapCanvas;
    public GameObject tilePrefab;
    // Use this for initialization
    void Start()
    {
        virtualTileMap = MapGenerator.generateNodes(8, 8);
        unityTileMap = MapGenerator.createUnityMap(virtualTileMap, tilePrefab, unityMapCanvas);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().SetScrollingBorders(unityTileMap);
        //MapGenerator.createUnityHeuristicDistance(unityTileMap, unityTileMap[3][2]);
        //MapGenerator.setUnityParentPath(unityTileMap[3][2], unityTileMap[3][3]);
        //MapGenerator.setUnityParentPath(unityTileMap[3][3], unityTileMap[3][4]);
        //MapGenerator.setUnityParentPath(unityTileMap[3][4], unityTileMap[4][5]);
        //MapGenerator.displayUnityNeighbours(unityTileMap[6][2]);
        MapGenerator.calculateUnityPath(unityTileMap[1][1], unityTileMap[6][6],unityTileMap);
    }
}
