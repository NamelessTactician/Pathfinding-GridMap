﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UnityNode : MonoBehaviour {

    public List<UnityNode> neighbours = new List<UnityNode>();
    public UnityNode parentPath;
    public GameObject topArrow;
    public GameObject botArrow;
    public GameObject leftArrow;
    public GameObject rightArrow;
    public GameObject RTArrow;
    public GameObject RBArrow;
    public GameObject LTArrow;
    public GameObject LBArrow;
    public Text f;
    public Text g;
    public Text h;
    public Text m;
    public Text x;
    public Text y;
    private bool passable = true;

    public int F
    {
        get{return int.Parse(f.text); }
        set{ f.text = value.ToString(); }
    }
    public int G
    {
        get { return int.Parse(g.text); }
        set
        {
            g.text = value.ToString();
            Update_F_value();
        }
    }
    public int H
    {
        get { return int.Parse(h.text); }
        set
        {
            h.text = value.ToString();
            Update_F_value();
        }
    }
    public int M
    {
        get { return int.Parse(m.text); }
        set
        {
            m.text = value.ToString();
            Update_F_value();
        }
    }
    public int X
    {
        get { return int.Parse(x.text); }
        set { x.text = value.ToString(); }
    }
    public int Y
    {
        get { return int.Parse(y.text); }
        set { y.text = value.ToString(); }
    }
    public bool Passable
    {
        get { return passable; }
        set { passable = value; }
    }

    public UnityNode(int xValue,int yValue,int moveCost, bool passAble)
    {
        this.X = xValue;
        this.Y = yValue;
        M = moveCost;
        passable = passAble;
    }

    void Update_F_value()
    {
        this.F = this.G + this.H;
    }
}
