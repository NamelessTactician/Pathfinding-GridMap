﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class UIpanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{
    public Canvas canvas;
    CanvasGroup canvGroup;
    float time;
    float delayOfFading = 4f;
    bool invisible;
	// Use this for initialization
	void Start () {
        canvGroup = GetComponent<CanvasGroup>();
        time = Time.time + delayOfFading;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > time && invisible == false)
        {
            //start fading out
            StartCoroutine(FadeOut());
        }
	}

    IEnumerator FadeOut()
    {
        while(canvGroup.alpha > 0)
        {
            canvGroup.alpha -= Time.deltaTime/2;
            yield return null;
        }
        invisible = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        canvGroup.alpha = 1;
        invisible = false;
        time = Time.time + delayOfFading;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
       
    }
}
